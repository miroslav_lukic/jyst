# Presentation project.

### Installing and starting project locally

When you clone project from Bitbucket repository first you need to run ```npm install``` command and after that ```npm run watch``` to start project.

## Built With

* [HTML5](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5) - HTML5 markup code is valid and semantic (W3C Standard)
* [CSS3](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS3) - Flexbox
* [BEM Methodology](http://getbem.com/) - Block Element Modifier is a methodology that helps you to create reusable components and code sharing in front-end development
* [Laravel Mix](https://laravel.com/docs/5.8/mix) - Laravel Mix provides a fluent API for defining Webpack build steps for your Laravel application using several common CSS and JavaScript pre-processors. Through simple method chaining, you can fluently define your asset pipeline.

## Author

* **Miroslav Lukic**  - [Website](http://www.miroslavlukic.info)


